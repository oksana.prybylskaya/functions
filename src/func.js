const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string' || isNaN(str1) || isNaN(str2)) {
    return false;
  }
  return (Number(str1) + Number(str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countPosts = 0;
  let countComments = 0;
  for (const post of listOfPosts) {
    if (post.author === authorName) {
      countPosts++;
    }
    if ('comments' in post) {
      for (const comment of post.comments) {
        if (comment.author === authorName) {
          countComments++;
        }    
      }
    }
  }
  return `Post:${countPosts},comments:${countComments}`;
};

const tickets=(people)=> {
  const TICKET = 25;
  let bank = 0;
  for (const bill of people) {
    if (bank < Number(bill) - TICKET) {
      return 'NO';
    }
    bank += TICKET;
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
